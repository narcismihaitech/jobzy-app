const colors = {
    white: '#ffffff',
    red: '#FF6666',
    borderColor: '#F5FCFF',
    heavyGrey: '#4D4D4D',
    mediumGrey: '#999999',
    lightGrey: '#B3B3B3',
    backgroundColor: '#132B45',
    black: '#000000'
  };
 
 export default colors;