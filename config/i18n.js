import i18n from 'i18n-js';
import {Platform, NativeModules} from 'react-native';

import ro from './locales/ro.json';
import en from './locales/en.json';

i18n.defaultLocale = 'en';
//iOS 13 workaround
if (Platform.OS === 'ios') {
    const majorVersionIOS = parseInt(Platform.Version, 10);
    if (majorVersionIOS == 13 && (typeof i18n.locale === 'undefined'))
        i18n.locale = 'en';
    else
        i18n.locale = NativeModules.SettingsManager.settings.AppleLocale.substring(0,2);
}
else {
    i18n.locale = NativeModules.I18nManager.localeIdentifier.substring(0,2);
}
i18n.fallbacks = true;
i18n.translations = { ro, en };

export default i18n;
