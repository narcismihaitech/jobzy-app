import React, {Component} from 'react';
import { Text, TextInput, FlatList, ImageBackground, Linking, View, SafeAreaView, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { ListItem, Button } from 'react-native-elements';
import colors from '../../config/colours.js'
import Tooltip from 'react-native-walkthrough-tooltip';
import {fetchJobTitles} from '../../redux/ActionCreators';
import {connect} from 'react-redux';
import api_key from '../../config/google_api_key';
import i18n from '../../config/i18n.js';
import DEBUG from '../../config/debug.js';
import Analytics from 'appcenter-analytics';
import Crashes from 'appcenter-crashes';
import { TouchableHighlight } from 'react-native-gesture-handler';
import styles from './styles.js';


const mapStateToProps = state => {
    return {
        jobTitles: state.jobTitles
    }
}

const mapDispatchToProps = dispatch => ({
    fetchJobTitles: () => dispatch(fetchJobTitles())
});

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            jobInputValue: '',
            addressInputValue: '',
            showJobsDropdown: false,
            showAddressesDropdown: false,
            jobsList: this.props.jobTitles.results,
            bucLatitude: '44.4354853',
            bucLongitude: '26.1026157',
            maxRadius: 18000,
            addressPredictions: [],
            showErrorMessage: false,
            errorMessage: '',
            showTooltip: false
         };
      }
    

    componentDidMount() {
        this.props.fetchJobTitles();
        this.checkAnalyticsEnabled();
    }

    checkAnalyticsEnabled = async() => {
        var enabled = await Analytics.isEnabled();
        if (!enabled)
            await Analytics.setEnabled(true);

        const crashesEnabled = await Crashes.isEnabled();
        if (!crashesEnabled) 
            await Crashes.setEnabled(true);
    }

    checkJobsDropdown = (jobInputValue) => {
        if (jobInputValue.length > 1) {
            try {
                var filteredJobsList = this.props.jobTitles.jobTitles.results.filter(function (element) {
                    return (element.toLowerCase().includes(jobInputValue.toLowerCase()));
                });
                if (filteredJobsList.length > 0) {
                    this.setState({
                        showJobsDropdown: true,
                        jobsList: filteredJobsList
                    });
                }
                else
                    this.setState({showJobsDropdown: false});
            }
            catch (err) {
                this.setState({
                    showErrorMessage: true,
                    errorMessage: i18n.t('homepage.genericError'),
                    showJobsDropdown: false
                })
            }
        }
        else {
            this.setState({showJobsDropdown: false});
        }
    }

    hideJobsDropdown = () => {
        this.setState({showJobsDropdown: false});
    }

    hideAddressesDropdown = () => {
        this.setState({showAddressesDropdown: false});
    }


    checkTooltip = async () => {
        var count = await this.getCount();
        if(count !== null) {
            if (count === '0') {
                this.setState({
                    showTooltip: true
                })
                try {
                    await AsyncStorage.setItem('countTooltipShown', '1');
                } catch (e) {
                    console.log(e);
                }
                }
                else {
                    this.setState({
                        showTooltip: false
                    })
                }

        }
        else {
            try {
                await AsyncStorage.setItem('countTooltipShown', '0');
                this.setState({
                    showTooltip: true
                })
            } catch (error) {
                console.log(e.message);
                }
        }
    }

    getCount = async () => {
        let countTooltip = '';
        try {
            countTooltip = await AsyncStorage.getItem('countTooltipShown');
        } catch (error) {
          // Error retrieving data
          console.log(error.message);
        }
      
        return countTooltip;
    }
    
    async onChangeAddress(address) {
        this.setState({addressInputValue: address});
        this.checkTooltip();
        if (address.length > 1 && !DEBUG) {
            this.setState({ address });
            const apiUrl = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=' + api_key +
            '&input=' + address + '&types=geocode&location=' + this.state.bucLatitude + ',' + this.state.bucLongitude + '&radius=' + this.state.maxRadius;
            try {
                const result = await fetch(apiUrl);
                const json = await result.json();
                var addresses = json.predictions;
                var filteredAddresses = [];
                var addressToAdd;
                for (var i = 0, prediction; prediction = addresses[i]; i++) {
                    if (prediction.description.includes('Bucharest') || prediction.description.includes('Ilfov') || prediction.description.includes('București')) {
                        addressToAdd = prediction.description.substring(0,prediction.description.indexOf(","));//use only the street name from the prediction
                        filteredAddresses.push(addressToAdd);	
                    }
                }
                if (filteredAddresses.length > 0) {
                    this.setState({
                        addressPredictions: filteredAddresses,
                        showAddressesDropdown: true,
                        refresh: !this.state.refresh
                    })
                }
                else {
                    this.setState({showAddressesDropdown: false});
                }
            } catch (err) {
                console.log(err);
                this.setState({
                    showErrorMessage: true,
                    errorMessage: i18n.t('homepage.genericError'),
                    showAddressesDropdown: false
                })
            }
        }
        else {
            this.setState({showAddressesDropdown: false});
        }
      }

    checkAddressField = () => {
        if (this.state.addressInputValue.length <= 1 ) {
            this.setState({
                showErrorMessage: true,
                errorMessage: i18n.t('homepage.enterAddress')
            })
            Analytics.trackEvent('search_nok', { Category: 'Search'});
        }
        else {
            if (DEBUG) {
                this.props.navigation.navigate('Jobs', {
                    jobTitle: this.state.jobInputValue,
                    address: this.state.addressInputValue,
                });
            }
            else {
                const apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.state.addressInputValue + '&key=' + api_key;
                fetch(apiUrl)
                .then(response => response.json())
                .then(data => {
                if (data.status === 'OK') {
                    this.setState({
                        showErrorMessage: false,
                        errorMessage: ''
                    })
                    Analytics.trackEvent('search_ok', { Category: 'Search'});
                    this.props.navigation.navigate('Jobs', {
                        jobTitle: this.state.jobInputValue,
                        address: this.state.addressInputValue,
                    });
                }
                else if (data.status === 'ZERO_RESULTS'){
                    this.setState({
                        showErrorMessage: true,
                        errorMessage: i18n.t('homepage.invalidAddress')
                    })
                }
                else {
                    this.setState({
                        showErrorMessage: true,
                        errorMessage: i18n.t('homepage.genericError')
                    })
                }
                })
                .catch(error => console.log(error))
            }
            
        }
      }

      contactUs = async () => {
        
        const url = 'mailto:' + i18n.t('homepage.recipient') + '?subject=' + i18n.t('homepage.subject');
        console.log('url is: ' + url);
        const canOpen = await Linking.canOpenURL(url);
        if (canOpen) {
            Linking.openURL(url);
        }
        else {
            this.setState({
                showErrorMessage: true,
                errorMessage: i18n.t('homepage.genericError')
            })
        }
      }

      openTerms = () => {
        this.props.navigation.navigate('Terms');
      }
  
      render() {
        return (
        
        <ImageBackground style={styles.bkgImage} source={require('../../assets/homepage_background.jpg')}>
            <SafeAreaView>
                <Text style={styles.header}>{i18n.t('appName')}</Text>
                <Text style={styles.descText1}>{i18n.t('homepage.header1')}</Text>
                <Text style={styles.descText2}>{i18n.t('homepage.header2')}</Text>
            </SafeAreaView>
            <TextInput 
                style={styles.jobTextInput}
                value={this.state.jobInputValue}
                placeholder={i18n.t('homepage.jobInput')}
                onChangeText={(jobInputValue) => this.setState({jobInputValue}, this.checkJobsDropdown(jobInputValue))}/>
                    <FlatList
                        data={this.state.jobsList}
                        style={this.state.showJobsDropdown ? styles.jobsDropdownStyle : styles.dropdownHiddenStyle}
                        contentContainerStyle={{borderRadius: 6, overflow: 'hidden'}}
                        renderItem={({ item }) => (
                        <ListItem
                            title={item}
                            containerStyle={{paddingBottom: -4}}
                            titleProps={{ style: styles.dropdownItemStyle}}
                            onPress={() => this.setState({jobInputValue: item}, this.hideJobsDropdown())}
                        />
                    )}
                    keyExtractor={item => item}
                />
            <Tooltip
                    animated={true}
                    isVisible={this.state.showTooltip}
                    onRequestClose={this.checkTooltip}
                    content={
                        <Text 
                        style={styles.tooltipStyle}
                        onPress={this.checkTooltip}>
                            {i18n.t('homepage.tooltip')}
                        </Text>}
                    placement="bottom"
                >
                <TextInput 
                    style={styles.addressTextInput}
                    value={this.state.addressInputValue}
                    placeholder={i18n.t('homepage.addressInput')}
                    onChangeText={addressInputValue => this.onChangeAddress(addressInputValue)}
                    onFocus={this.checkTooltip}
                    />
            </Tooltip>
                <FlatList
                    data={this.state.addressPredictions}
                    extraData={this.state}
                    style={this.state.showAddressesDropdown ? styles.addressDropdownStyle : styles.dropdownHiddenStyle}
                    contentContainerStyle={{borderRadius: 6, overflow: 'hidden'}}
                    renderItem={({ item, index }) => (
                        <ListItem
                            title={item}
                            containerStyle={{paddingBottom: -4}}
                            titleProps={{style: styles.dropdownItemStyle}}
                            onPress={() => this.setState({addressInputValue: item}, this.hideAddressesDropdown())}
                        />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            <Text style={this.state.showErrorMessage ? styles.errorMessageStyle : styles.errorMessageHidden}>{this.state.errorMessage}</Text>
            <Button 
                title={i18n.t('homepage.searchLabel')}
                type="outline"
                underlayColor={colors.red}
                titleStyle={styles.buttonTitleStyle}
                color={colors.red}
                buttonStyle={styles.buttonStyle}
                onPress={this.checkAddressField}
                />
            <View style={styles.bottomViewStyle}>
                <TouchableHighlight 
                    underlayColor={'transparent'} 
                    onPress={this.contactUs} > 
                    <Text 
                        style={styles.contactStyle}
                        onPress={this.contactUs}>
                        {i18n.t('homepage.contact')}
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight 
                    underlayColor={'transparent'} 
                    onPress={this.openTerms}>
                    <Text 
                        style={styles.termsStyle}
                        onPress={this.openTerms}
                        underlayColor={'transparent'}>
                        {i18n.t('homepage.termeni')}
                    </Text>
                </TouchableHighlight>
            </View>
            
        </ImageBackground>
        
        );
      }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);