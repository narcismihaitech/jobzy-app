import {StyleSheet, Platform} from 'react-native';
import colors from '../../config/colours.js'
import { isIphoneX } from '../../utils/is-iphone-x.js';

export default StyleSheet.create({
    bkgImage: {
        width: '100%', 
        height: '100%',
        flex: 1,
        alignItems: 'center'
  
    },
    header: {
        marginTop: 20,
        fontSize: 40,
        fontWeight: '700',
        color: colors.white,
        zIndex: 2,
        alignSelf: 'center'
    },
    descText1: {
        marginTop: 40,
        fontSize: 18,
        color: colors.white,
        zIndex: 2,
        alignSelf: 'center'
    },
    descText2: {
        fontSize: 18,
        color: colors.white,
        zIndex: 2,
        alignSelf: 'center'
    },
    jobTextInput: {
        height: 40,
        width: 300,
        marginTop: 40,
        borderWidth: 1,
        borderColor: colors.borderColor,
        textAlign: 'center',
        borderRadius: 6,
        fontSize: 18,
        backgroundColor: colors.white,
        color: colors.heavyGrey,
        zIndex: 2
    },
    addressTextInput: {
        height: 40,
        width: 300,
        marginTop: 20,
        borderWidth: 1,
        borderColor: colors.borderColor,
        textAlign: 'center',
        borderRadius: 6,
        fontSize: 18,
        color: colors.heavyGrey,
        backgroundColor: colors.white
    },
    buttonStyle: {
        height: 40,
        width: 300,
        marginTop: 20,
        borderRadius: 6,
        borderColor: colors.red,
        borderWidth: 2,
        zIndex: 0
    },
    buttonTitleStyle: {
        color: colors.white,
        marginTop: -3
        
    },
    jobsDropdownStyle: {
        width: 300,
        height: 140,
        ...Platform.select({
            ios: {
              marginTop: isIphoneX() ? 280 : 260,
            },
            android: {
                marginTop: 250
            }
          }),
        borderRadius: 6,
        overflow: 'hidden',
        zIndex: 1,
        position:'absolute'
    },
    addressDropdownStyle: {
        width: 300,
        height: 200,
        ...Platform.select({
            ios: {
              marginTop: isIphoneX() ? 340 : 320,
            },
            android: {
                marginTop: 310
            }
          }),
        borderRadius: 6,
        overflow: 'hidden',
        zIndex: 1,
        position:'absolute'
    },
    dropdownHiddenStyle: {
        width: 0,
        height: 0,
        marginTop: 240,
        zIndex: 1,
        position:'absolute'
    },
    dropdownItemStyle: {
        fontSize: 18,
        color: '#B3B3B3'
    },
    errorMessageStyle: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.red,
        marginTop: 8
    },
    errorMessageHidden: {
        width: 0,
        height: 0,
        marginTop: 10
    },
    bottomViewStyle: {
        flexDirection: 'row', 
        bottom: 20,
        ...Platform.select({ 
            ios: { 
                right: 40,
            }, 
            android: { 
                right: 55,
            }, 
        }), 
        position: 'absolute'
    },
    contactStyle: {
        fontSize: 14,
        color: colors.white,
        fontWeight: 'bold',
        marginRight: 15
    },
    termsStyle: {
        fontSize: 14,
        color: colors.white,
        fontWeight: 'bold',
    },
    tooltipStyle: {
        width: 200,
        color: colors.heavyGrey,
        textAlign: 'center'
    }
    
  });