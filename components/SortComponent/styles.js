import {StyleSheet} from 'react-native';
import colors from '../../config/colours.js'

export default StyleSheet.create({
    parentView: {
        flex:1,
        marginTop: 10,
        marginLeft: 20, 
        justifyContent: 'flex-start'
    },
    touchableStyle: {
        flex: 0.25
    },
    iconStyle: {
        height: 40,
        width: 40
    },
    viewStyle: {
        flexDirection: 'row'
    },
    sortText: {
        marginLeft: 5, 
        marginTop: 5, 
        fontSize: 20, 
        color: colors.heavyGrey
    }
});