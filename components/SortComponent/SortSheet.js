
import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight} from 'react-native';
import i18n from '../../config/i18n.js';
import colors from '../../config/colours.js'
import styles from './styles.js';

class SortSheet extends Component {
    render() {
        return (
            <View style={styles.parentView}>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={styles.touchableStyle}
                    onPress={() => this.props.sortOrder(this.props.sortArray[0])}>
                    <View style={styles.viewStyle}> 
                        <Image 
                            source={require('../../assets/date.png')} 
                            style={styles.iconStyle}/>
                        <Text style={styles.sortText}>{i18n.t('jobspage.sort_date')}</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={styles.touchableStyle}
                    onPress={() => this.props.sortOrder(this.props.sortArray[1])}>
                    <View style={styles.viewStyle}> 
                        <Image 
                            source={require('../../assets/car.png')} 
                            style={styles.iconStyle}/>
                        <Text style={styles.sortText}>{i18n.t('jobspage.sort_car')}</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={styles.touchableStyle}
                    onPress={() => this.props.sortOrder(this.props.sortArray[2])}>
                    <View style={styles.viewStyle}> 
                        <Image 
                            source={require('../../assets/tram.png')} 
                            style={styles.iconStyle}/>
                        <Text style={styles.sortText}>{i18n.t('jobspage.sort_tram')}</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={styles.touchableStyle}
                    onPress={() => this.props.sortOrder(this.props.sortArray[3])}>
                    <View style={styles.viewStyle}> 
                        <Image 
                            source={require('../../assets/walk.png')} 
                            style={styles.iconStyle}/>
                        <Text style={styles.sortText}>{i18n.t('jobspage.sort_walk')}</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default SortSheet;