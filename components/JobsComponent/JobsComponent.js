import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, ScrollView, FlatList, Platform, NativeModules, Button} from 'react-native';
import {geocodeAddress} from '../../redux/geocodeAddress';
import Panel from '../PanelComponent/PanelComponent';
import { Loading } from '../LoadingComponent/LoadingComponent.js';
import i18n from '../../config/i18n.js';
import Analytics from 'appcenter-analytics';
import styles from './styles.js';
import RBSheet from "react-native-raw-bottom-sheet";
import SortSheet from '../SortComponent/SortSheet';

const mapStateToProps = state => {
    return {
        jobs: state.jobs
    }
}

const mapDispatchToProps = dispatch => ({
        geocodeAddress: (address, jobTitle) => dispatch(geocodeAddress(address, jobTitle))
});

function formatDate(rawDate) {
    var splitDate = rawDate.split(" ");
    var locale = Platform.OS === 'ios' ? NativeModules.SettingsManager.settings.AppleLocale.substring(0,2) : NativeModules.I18nManager.localeIdentifier.substring(0,2);

    switch(splitDate[1]) {
        case "Jan":
            splitDate[1] = locale === 'ro' ? "ianuarie" : "January";
            break;
        case "Feb":
            splitDate[1] = locale === 'ro' ? "februarie" : "February";
            break;
        case "Mar":
            splitDate[1] = locale === 'ro' ? "martie" : "March";
            break;
        case "Apr":
            splitDate[1] = locale === 'ro' ? "aprilie" : "April";
            break;
        case "May":
            splitDate[1] = locale === 'ro' ? "mai" : "May";
            break;
        case "Jun":
            splitDate[1] = locale === 'ro' ? "iunie" : "June";
            break;
        case "Jul":
            splitDate[1] = locale === 'ro' ? "iulie" : "July";
            break;
        case "Aug":
            splitDate[1] = locale === 'ro' ? "august" : "August";
            break;
        case "Sep":
            splitDate[1] = locale === 'ro' ? "septembrie" : "September";
            break;
        case "Oct":
            splitDate[1] = locale === 'ro' ? "octombrie" : "October";
            break;
        case "Nov":
            splitDate[1] = locale === 'ro' ? "noiembrie" : "November";
            break;
        case "Dec":
            splitDate[1] = locale === 'ro' ? "decembrie" : "December";
            break;
        default: 
            splitDate[1] = splitDate[1];
    }
    var formattedDate;

    if (locale === 'ro')
        formattedDate = splitDate[2] + " " + splitDate[1];
    else
        formattedDate = splitDate[1] + " " + splitDate[2];

    return formattedDate;
}

function formatJobDescription(jobDescription) {
    var formattedJobDescription = jobDescription.replace(/<li>/g,'     \u2022 ').replace(/<ul>|<\/ul>|<p>|<\/p>|<br\/>|<br>/g,'').replace(/<br\/>|<\/li>/g,'\n');
    return formattedJobDescription;
}

function RenderJobs(props) {
    
    var mapObj = {
        mins:"min",
        hours:"ore",
        hour: "ora"
    };
    const renderJobItem = ({item}) => {
        var  durationCarApi, durationPublicTransportApi, durationWalkApi, formattedApiDate, formattedJobDescription;
        durationCarApi = item.duration_driving == null ? "N/A" : item.duration_driving;
        durationCarApi = durationCarApi.replace(/mins|hours|hour/gi, function(matched){
            return mapObj[matched];
          });
        durationCarApi = durationCarApi.replace(" 0 min", "N/A");

        durationPublicTransportApi = item.duration_transit == null ? "N/A" : item.duration_transit;
		durationPublicTransportApi = durationPublicTransportApi.replace(/mins|hours|hour/gi, function(matched){
		  return mapObj[matched];
		});
        durationPublicTransportApi = durationPublicTransportApi.replace(" 0 min", "N/A");

        durationWalkApi = item.duration_walking == null ? "N/A" : item.duration_walking;
		durationWalkApi = durationWalkApi.replace(/mins|hours|hour/gi, function(matched){
		  return mapObj[matched];
		});
        durationWalkApi = durationWalkApi.replace(" 0 min", "N/A");
        formattedApiDate = formatDate(item.date);
        formattedJobDescription = formatJobDescription(item.formatedDescription);
        return (
            <Panel 
                jobTitle={item.jobtitle}
                company={item.company}
                durationCar={durationCarApi}
                durationTram={durationPublicTransportApi}
                durationWalking={durationWalkApi}
                dateAdded={formattedApiDate}
                onPress={() => 
                    {
                    props.navigation.navigate('JobDetails', {
                        jobTitle: item.jobtitle,
                        company: item.company,
                        durationCar: durationCarApi,
                        durationTram: durationPublicTransportApi,
                        durationWalking: durationWalkApi,
                        jobDescription: formattedJobDescription,
                        applyUrl: item.applyUrl
                })
                    Analytics.trackEvent('job_clicked', { Category: 'Jobs', JobName: item.jobtitle});
                    }
                }/>
        );
    }

    if (props.isLoading) {
        return (
                <Loading />
        );
    }
    else if (props.errMess) {
        if (props.errMess.includes('204')) {//no jobs available
            Analytics.trackEvent('no_jobs_found', { Category: 'Jobs'});
            return (
                <View style={styles.errorView}>
                    <Text style={styles.errorTextStyle}>{i18n.t('jobspage.noJobsText')}</Text>
                </View>
            )
        }
        else {
            return(
                <View style={styles.errorView}>
                    <Text style={styles.errorTextStyle}>{i18n.t('jobspage.errorText')}</Text>
                </View>
            );
        }
    }
    else {
        console.log('Before flatList, sortOrderChanged: ' + props.sortOrderProps);
        return (
            <FlatList 
                    data={props.jobsData}
                    extraData={props.sortOrderProps}
                    renderItem={renderJobItem}
                    keyExtractor={(item, index) => item._id}
                    style={{marginTop: 10}}
                    />
        );
    }
    
}

class Jobs extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            jobTitle: this.props.navigation.getParam('jobTitle', ''),
            address: this.props.navigation.getParam('address', 'error'),
            jobs: this.props.jobs.jobs,
            sortOrderChanged: false,
            sortArray: [0,1,2,3],
            selectedSortOrder: 0 //default is sort_driving
         };
      }

    componentDidMount() {
        this.props.geocodeAddress(this.state.address, this.state.jobTitle);
        const handlePress = () => {
            if (this.props.jobs.errMess === null) {
                Analytics.trackEvent('map_clicked');
                this.props.navigation.navigate('MapComponent', {jobsData: this.props.jobs.jobs});
            }
        }

        const sortIconPressed = () => {
            this.RBSheet.open();
        }

        this.props.navigation.setParams({ mapPress: handlePress, sortPress: sortIconPressed });
    }

    handleSorting = (dataFromChild) => {
        console.log('Sort order clicked: ' + dataFromChild);
        this.RBSheet.close();
        this.setState({
            sortOrderChanged: !this.state.sortOrderChanged,// + 1,
            selectedSortOrder: dataFromChild
        });
        
    }

    formatJobsData = (jobsData) => {
        var json = JSON.parse(JSON.stringify(jobsData));
        var count = 0;//the _id we get from the server is random, so we have to use our own
        var sort_array = [];
        for (var _id in json) {
            sort_array.push({
                _id: count++,
                jobtitle: json[_id].jobtitle,
                company: json[_id].company,
                duration_driving_value:json[_id].duration_driving.value,
                duration_transit_value: json[_id].duration_transit.value,
                duration_walking_value: json[_id].duration_walking.value,
                duration_driving:json[_id].duration_driving.text,
                duration_transit:json[_id].duration_transit.text,
                duration_walking:json[_id].duration_walking.text,
                date: json[_id].date,
                formatedDescription: json[_id].formatedDescription,
                applyUrl: json[_id].applyUrl
            });
        }

        if (this.state.selectedSortOrder === this.state.sortArray[0])
            sort_array.sort(function(x,y){return new Date(y.date) - new Date(x.date)});
        else if (this.state.selectedSortOrder === this.state.sortArray[1]) 
            sort_array.sort(function(x,y){return x.duration_driving_value - y.duration_driving_value});
        else if (this.state.selectedSortOrder === this.state.sortArray[2]) 
            sort_array.sort(function(x,y){return x.duration_transit_value - y.duration_transit_value});
        else 
            sort_array.sort(function(x,y){return x.duration_walking_value - y.duration_walking_value});
        
        return sort_array;
    }


    render() {
        return(
            <ScrollView contentContainerStyle={styles.bkg}>
                <RenderJobs 
                    jobsData={this.formatJobsData(this.state.jobs)}
                    isLoading={this.props.jobs.isLoading}
                    errMess={this.props.jobs.errMess}
                    navigation={this.props.navigation}
                    sortOrder={this.state.selectedSortOrder}
                    sortArray={this.state.sortArray}
                    sortOrderProps={this.state.sortOrderChanged}
                    />
                <View>
                    <RBSheet
                        ref={ref => {this.RBSheet = ref;}}
                        height={200}
                        duration={250}>
                    <SortSheet
                        sortOrder={this.handleSorting}
                        sortArray={this.state.sortArray}/>
                    </RBSheet>
                </View>
            </ScrollView>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Jobs);