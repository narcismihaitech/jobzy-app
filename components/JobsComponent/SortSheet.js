
import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight} from 'react-native';
import i18n from '../../config/i18n.js';
import colors from '../../config/colours.js'
import {sort_array} from './JobsComponent';


class SortSheet extends Component {
    render() {
        var currentSortOrder, closeSheet = true;
        return (
            <View style={{flex:1, marginTop: 10, marginLeft: -60,  justifyContent: 'flex-start'}}>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={{flex: 0.25}}
                    onPress={() => this.props.sortOrder(sort_array[0])}>
                    <View style={{flexDirection: 'row'}}> 
                        <Image 
                            source={require('../../assets/car.png')} 
                            style={{height: 40, width: 40}}/>
                        <Text style={{marginLeft: 5, marginTop: 5, fontSize: 20, color: colors.heavyGrey}}>{i18n.t('jobspage.sort_car')}</Text>
                    </View>
                </TouchableHighlight>
                <TouchableHighlight
                    underlayColor="transparent"
                    style={{flex: 0.25}}
                    onPress={() => this.props.sortOrder(sort_array[1])}>
                    <View style={{flexDirection: 'row'}}> 
                        <Image 
                            source={require('../../assets/tram.png')} 
                            style={{height: 40, width: 40}}/>
                        <Text style={{marginLeft: 5, marginTop: 5, fontSize: 20, color: colors.heavyGrey}}>{i18n.t('jobspage.sort_tram')}</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

export default SortSheet;