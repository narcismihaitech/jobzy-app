import {StyleSheet} from 'react-native';
import colors from '../../config/colours.js'

export default StyleSheet.create({
    bkg: {
        backgroundColor: colors.backgroundColor,
        flex: 1
    },
    buttonStyle: {
        height: 30,
        width: 110,
        borderRadius: 6,
        borderColor: colors.red,
        borderWidth: 2,
        alignSelf: 'flex-end'
    },
    buttonTitleStyle: {
        color: colors.red,
        marginTop: -9
        
    },
    errorTextStyle: {
        color: colors.white,
        fontSize: 18,
        width: 300,
        textAlign: 'center',
        marginTop: 40
    },
    errorView:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    }
});