import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import colours from '../../config/colours.js';
import i18n from '../../config/i18n.js';
import styles from './styles.js';

export const Loading = () => {
    return(
        <View style={styles.loadingView}>
            <ActivityIndicator size="large" color={colours.white} />
            <Text style={styles.loadingText}>{i18n.t('jobspage.loadingText')}</Text>
        </View>
    );
}
