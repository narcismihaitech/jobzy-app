import colours from '../../config/colours.js';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    loadingView:{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    loadingText: {
        color: colours.white,
        fontSize: 18,
        width: 300,
        textAlign: 'center',
        marginTop: 10
    }
})