import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './HomeComponent/HomeComponent.js';
import Terms from './TermsComponent/TermsComponent.js';
import Jobs from './JobsComponent/JobsComponent.js';
import JobDetails from './JobDetailsComponent/JobDetailsComponent.js';
import MapComponent from './MapComponent/MapComponent.js';
import {TouchableHighlight, Image, View} from 'react-native';
import {Text} from 'react-native';
import i18n from '../config/i18n.js';

export const navHeaderRight = (navigation) => {
  const handlePress = navigation.getParam('mapPress', null);
  const sortIconPressed = navigation.getParam('sortPress', null);

  return (
    <View style={{marginRight: 5, flexDirection: 'row'}}> 
      <TouchableHighlight
        underlayColor="transparent"
        style={{marginTop: 8}}
        onPress={sortIconPressed}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{marginTop:5}}>{i18n.t('navigator.sort')}</Text>
          <Image 
            source={require('../assets/sort-icon.png')} 
            style={{height: 30, width: 30}}
          />
        </View>
      </TouchableHighlight>
      <TouchableHighlight
        underlayColor="transparent"
        onPress={handlePress}>
        <Image 
          source={require('../assets/map.png')} 
          style={{height: 40, width: 40}}
        />
      </TouchableHighlight>
    </View>
  );
}

const Navigator = createStackNavigator({
    Home: { 
      screen: Home,
      navigationOptions: {
        header: null
      } 
    },
    Terms: { screen: Terms },
    Jobs: { 
      screen: Jobs,
      navigationOptions: ({navigation}) => ({
        headerRight: navHeaderRight(navigation)
      })
    },
    JobDetails: { 
      screen: JobDetails,
      navigationOptions: {
        header: null
      } 
    },
    MapComponent: { screen: MapComponent}
  });

  const AppNavigator = createAppContainer(Navigator);

  export default AppNavigator;