import React, {Component} from 'react';
import {ScrollView, Text, ImageBackground, View, Image, StatusBar, TouchableHighlight, Platform, Linking} from 'react-native';
import colors from '../../config/colours.js';
import i18n from '../../config/i18n.js';
import { Button } from 'react-native-elements';
import Analytics from 'appcenter-analytics';
import { isIphoneX } from '../../utils/is-iphone-x.js';
import styles from './styles.js';

class JobDetails extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            jobTitle: this.props.navigation.getParam('jobTitle', 'N/A'),
            company: this.props.navigation.getParam('company', 'N/A'),
            durationCar: this.props.navigation.getParam('durationCar', 'N/A'),
            durationTram: this.props.navigation.getParam('durationTram', 'N/A'),
            durationWalking: this.props.navigation.getParam('durationWalking', 'N/A'),
            jobDescription: this.props.navigation.getParam('jobDescription', 'N/A'),
            applyUrl: this.props.navigation.getParam('applyUrl', 'N/A'),
            headerHeight: 0,
            headerBorderWidth: 0,
            headerMarginTop: 0,
            imageHeight: 200,
            jobTitleSize: 0,
            iconHeight: 0,
            iconWidth: 0
            
         };

         this.icons = {     
            'car'    : require('../../assets/car.png'),
            'tram'    : require('../../assets/tram.png'),
            'walk'    : require('../../assets/walk.png')
        };
      }

      openBrowser = () => {
        Linking.canOpenURL(this.state.applyUrl).then(supported => {
          if (supported) {
            Analytics.trackEvent('apply_clicked', { Category: 'Jobs', JobUrl: this.state.applyUrl});
            Linking.openURL(this.state.applyUrl);
          } else {
            console.log("Don't know how to open URI: " + this.state.applyUrl);
          }
        });
      };

      handleScroll(event) {
        if (Platform.OS === 'ios') {
            if (event.nativeEvent.contentOffset.y > 160 && this.state.jobDescription.length > 800) {
                this.setState({
                    headerHeight: 40,
                    headerBorderWidth: 1,
                    headerMarginTop: isIphoneX() ? 35 : 0,
                    iconHeight: 20,
                    iconWidth: 20,
                    imageHeight: 160,
                    jobTitleSize: 14
                }
                )
            }
            else {
                this.setState({
                    headerHeight: 0,
                    headerBorderWidth: 0,
                    headerMarginTop: 0,
                    iconHeight: 0,
                    iconWidth: 0,
                    imageHeight: 200,
                    jobTitleSize: 0
                })
            }
        }
       }


    render() {
        return(
            <View>
                <StatusBar hidden={Platform.OS === 'android' ? false : true} />
                <View style={styles.scrollViewStyle}>
                    <View style={[styles.headerStyle, {height: this.state.headerHeight, borderWidth: this.state.headerBorderWidth, marginTop: this.state.headerMarginTop}]}>
                        <View style={styles.jobHeaderStyle}>
                            <Text style={[styles.jobHeaderText, {fontSize: this.state.jobTitleSize}]} numberOfLines={1}>{this.state.jobTitle}</Text>
                        </View>
                        <View style={{flex: 0.1, width: 1}}>
                        </View>
                        <View style={styles.iconHeaderViewStyle}>
                                <TouchableHighlight 
                                    onPress={() => this.props.navigation.goBack(null)}
                                    underlayColor={'transparent'}>
                                    <Image 
                                        source={require('../../assets/close-icon-blue.png')} 
                                        style={[styles.iconHeaderStyle, {height: this.state.iconHeight, width: this.state.iconWidth}]}/>
                                </TouchableHighlight>
                        </View>
                    </View>
                    <ScrollView onScroll={this.handleScroll.bind(this)} scrollEventThrottle={32}>
                        <ImageBackground style={[styles.coverImage, {height: this.state.imageHeight}]} source={require('../../assets/homepage_background.jpg')}>
                            <View style={styles.iconViewStyle}>
                                <TouchableHighlight 
                                    onPress={() => this.props.navigation.goBack(null)}
                                    underlayColor={'transparent'}>
                                    <Image 
                                        source={require('../../assets/close-icon-white.png')} 
                                        style={styles.iconStyle}/>
                                </TouchableHighlight>
                            </View>
                            <Text style={styles.jobTitleStyle}>{this.state.jobTitle}</Text>
                            <Text style={styles.companyStyle}>{this.state.company}</Text>
                        </ImageBackground>
                        <View style={styles.durationViewStyle}>
                            <Image 
                                source={this.icons['car']} 
                                style={styles.durationIconStyle}/>
                            <Text style={styles.durationStyle}>{this.state.durationCar}</Text>
                            <Image 
                                source={this.icons['tram']} 
                                style={styles.durationIconStyle}/>
                            <Text style={styles.durationStyle}>{this.state.durationTram}</Text>
                            <Image 
                                source={this.icons['walk']} 
                                style={styles.durationIconStyle}/>
                            <Text style={styles.durationStyleWalk}>{this.state.durationWalking}</Text>
                        </View>
                        <Text style={styles.jobDescriptionStyle}>{this.state.jobDescription}</Text>
                    </ScrollView>
                </View>
                    <View style={styles.applyView}>
                            <Button 
                                title={i18n.t('jobspage.apply')}
                                type="outline"
                                underlayColor={colors.red}
                                titleStyle={styles.buttonTitleStyle}
                                color={colors.red}
                                buttonStyle={styles.buttonStyle}
                                onPress={this.openBrowser}
                            />
                    </View>
            </View>
        )
    }
}

export default JobDetails;