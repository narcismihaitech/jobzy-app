import {StyleSheet, Platform} from 'react-native';
import { isIphoneX } from '../../utils/is-iphone-x.js';
import colors from '../../config/colours.js'


export default StyleSheet.create({
    coverImage: {
        width: '100%', 
        height: 200,
        alignItems: 'center'
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconStyle: {
        marginTop: isIphoneX() ? 35 : 10,
        marginRight: 10,
        width: 20,
        height: 20,
        alignSelf: 'flex-end'
    },
    jobTitleStyle: {
        fontSize: 24,
        color: colors.white,
        marginHorizontal: 5,
        marginTop: 40,
        textAlign: 'center',
    },
    companyStyle: {
        fontSize: 18,
        color: colors.white,
        marginHorizontal: 5,
        marginTop: 5,
        textAlign: 'center',
    },
    durationViewStyle: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        marginVertical: 10,
        marginHorizontal: 10
    },
    durationStyle: {
        fontSize: 14,
        color: colors.heavyGrey,
        marginTop: 8,
        marginLeft: 2
    },
    durationStyleWalk: {
        fontSize: 14,
        color: colors.heavyGrey,
        marginTop: 8,
        marginLeft: -4
    },
    durationIconStyle: {
        width: 30,
        height: 30, 
        resizeMode:'contain'
    },
    scrollViewStyle: {
        height: '92%'
    },
    buttonTitleStyle: {
        color: colors.red,
        marginTop: -15
    },
    jobDescriptionStyle: {
        marginHorizontal: 10
    },
    applyView: {
        height: '8%',
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightGrey,
        ...Platform.select({ 
            ios: { 
                shadowColor: colors.lightGrey, 
                shadowOpacity: 0.5, 
            }, 
            android: { 
                elevation: 5 
            }, 
        })
    },
    buttonStyle: {
        height: 32,
        width: 110,
        marginTop: 10,
        marginBottom: 5,
        marginRight: 20,
        borderRadius: 6,
        borderColor: colors.red,
        borderWidth: 2,
        alignSelf: 'flex-end',
        ...Platform.select({ 
            ios: { 
                paddingTop: 8
            }, 
            android: { 
                paddingTop: 14
            }, 
        })
    },
    buttonTitleStyle: {
        color: colors.red,
        marginTop: -8
    },
    headerStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between', 
        borderColor: colors.white,
        backgroundColor: colors.white,
        borderColor: colors.lightGrey,
        ...Platform.select({ 
            ios: { 
                shadowColor: colors.lightGrey, 
                shadowOpacity: 0.5, 
            }, 
            android: { 
                elevation: 5 
            }, 
        })
    },
    iconViewStyle: {
        alignSelf: 'flex-end',
        justifyContent: 'flex-end'
    },
    iconHeaderViewStyle: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    jobHeaderStyle: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10
    },
    jobHeaderText: {
        color: colors.heavyGrey,
        textAlign: 'center',
        marginLeft: 10,
        fontWeight: 'bold'
    },
    iconHeaderStyle: {
        marginVertical: 10,
        marginRight: 10
    }
});