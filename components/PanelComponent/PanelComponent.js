
import React,{Component} from 'react';
import {StyleSheet,Text,View,Image,TouchableHighlight} from 'react-native';
import colors from '../../config/colours';
import i18n from '../../config/i18n.js';
import styles from './styles.js';

class Panel extends Component{
    constructor(props){
        super(props);

        this.state = {
            jobTitle: props.jobTitle,
            company: props.company,
            durationCar: props.durationCar,
            durationTram: props.durationTram,
            durationWalking: props.durationWalking,
            dateAdded: props.dateAdded,
            expanded: false,
            childrenHeight: 0,
            onPress: props.onPress
        };

        this.icons = {     
            'car'    : require('../../assets/car.png'),
            'tram'    : require('../../assets/tram.png'),
            'walk'    : require('../../assets/walk.png')
        };
    }

    render(){

        return (
            <View style={styles.container}>
                    <TouchableHighlight 
                        onPress={this.state.onPress}
                        underlayColor={colors.white}>
                        <View style={styles.cardStyle}>
                            <Text style={styles.jobTitleStyle}>{this.state.jobTitle}</Text>
                            <Text style={styles.companyStyle}>{this.state.company}</Text>
                            <View style={styles.durationViewStyle}>
                                <Image 
                                    source={this.icons['car']} 
                                    style={styles.iconStyle}/>
                                <Text style={styles.durationStyle}>{this.state.durationCar}</Text>
                                <Image 
                                    source={this.icons['tram']} 
                                    style={styles.iconStyle}/>
                                <Text style={styles.durationStyle}>{this.state.durationTram}</Text>
                                <Image 
                                    source={this.icons['walk']} 
                                    style={styles.iconStyle}/>
                                <Text style={styles.durationStyleWalk}>{this.state.durationWalking}</Text>
                            </View>
                            <Text>
                                <Text style={styles.dateStyle}>{i18n.t('jobspage.dateAdded')}</Text>
                                <Text style={styles.datePropsStyle}>{this.state.dateAdded}</Text>
                        </Text>
                        </View>
                    </TouchableHighlight>
            </View>
        );
    }
}

export default Panel;