import {StyleSheet} from 'react-native';
import colors from '../../config/colours.js';

export default StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginHorizontal: 0,
        marginVertical: 10,
        overflow:'hidden',
        width: '100%',
        flexDirection: 'column',
        width: '100%',
        marginHorizontal: 0
    },
    title: {
        flex: 1,
        padding: 10,
        color:'#2a2f43',
        fontWeight:'bold',
        fontSize: 10
    },
    buttonImage: {
        width: 300,
        height: 25
    },
    body: {
        paddingTop  : 0
    },
    durationViewStyle: {
        flexDirection: 'row',
        marginBottom: 10
    },
    iconStyle: {
        width:40,
        height:40, 
        resizeMode:'contain'
    },
    cardStyle: {
        marginVertical: 25,
        marginHorizontal: 15
    },
    jobTitleStyle: {
        fontSize: 22,
        color: colors.heavyGrey,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    companyStyle: {
        fontSize: 20,
        color: colors.lightGrey,
        marginBottom: 10,
    },
    durationStyle: {
        fontSize: 18,
        color: colors.heavyGrey,
        marginTop: 8,
        marginLeft: 2
    },
    durationStyleWalk: {
        fontSize: 18,
        color: colors.heavyGrey,
        marginTop: 8,
        marginLeft: -4
    },
    dateStyle: {
        fontSize: 14,
        color: colors.heavyGrey,
        fontStyle: 'italic'
    },
    datePropsStyle: {
        fontSize: 14,
        color: colors.heavyGrey,
        fontWeight: 'bold'
    },
    buttonStyle: {
        height: 30,
        width: 110,
        borderRadius: 6,
        borderColor: colors.red,
        borderWidth: 2,
        alignSelf: 'flex-end'
    },
    buttonTitleStyle: {
        color: colors.red,
        marginTop: 1,
        marginRight: 20
        
    }
});