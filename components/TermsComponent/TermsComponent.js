import React, {Component} from 'react';
import { Text, ScrollView } from 'react-native';
import styles from './styles.js';
import i18n from '../../config/i18n.js';



class Terms extends Component {

    render() {
        return(
            <ScrollView contentContainerStyle={styles.bkg}>
                <Text style={styles.title}>{i18n.t('terms.title')}</Text>
                <Text style={styles.paragraph}>{i18n.t('terms.abstract')}</Text>
                <Text style={styles.subtitle}>{i18n.t('terms.appContentTitle')}</Text>
                <Text style={styles.paragraph}>{i18n.t('terms.appContentParagraph')}</Text>
                <Text style={styles.subtitle}>{i18n.t('terms.dreptAplicabilTitle')}</Text>
                <Text style={styles.paragraph}>{i18n.t('terms.dreptAplicabilParagraph')}</Text>
                <Text style={styles.subtitle}>{i18n.t('terms.disponbilitateTitle')}</Text>
                <Text style={styles.paragraph}>{i18n.t('terms.disponbilitateParagraph')}</Text>
            </ScrollView>
        )
    }
}

export default Terms;