import colors from '../../config/colours.js';
import {StyleSheet} from 'react-native';


export default StyleSheet.create({
    bkg: {
        backgroundColor: colors.backgroundColor,
        alignItems: 'center'
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: colors.white,
        marginTop: 25
    },
    subtitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white,
        alignSelf: 'flex-start',
        marginLeft: 10,
        marginRight: 10
    },
    paragraph: {
        fontSize: 15,
        color: colors.white,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 25,
        textAlign: 'justify'
    }
});