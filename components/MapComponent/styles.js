import colors from '../../config/colours.js'
import {StyleSheet, Platform} from 'react-native';

export default StyleSheet.create({
    map: {
        flex: 1,
        ...StyleSheet.absoluteFillObject
    },
    calloutStyle: {
        flex: 1,
        flexDirection: 'column'
    },
    textHeaderStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleText: {
        color: colors.heavyGrey,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 14
    },
    descriptionTextView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    locationStyle: {
        position: 'absolute',
        bottom: 20,
        right: 20
    },
    touchStyle: {
        ...Platform.select({ 
            ios: { 
                shadowColor: colors.black, 
                shadowOffset: {
                    width: 0,
                    height: 7,
                },
                shadowOpacity: 0.41,
                shadowRadius: 9.11,
            }, 
            android: { 
                elevation: 20,
                backgroundColor: 'transparent',
                borderRadius: 40
            }, 
        })
    },  
    imageStyle: {
        backgroundColor: colors.white,
        borderRadius: 40,
        width: 50,
        height: 50
    }
   });