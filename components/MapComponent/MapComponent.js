import React, {Component} from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import {StyleSheet, TouchableHighlight, View, Text, Image, NativeModules, Platform, Alert, Linking} from 'react-native';
import styles from './styles.js';
import Geolocation from 'react-native-geolocation-service';
import i18n from '../../config/i18n.js';
import Permissions from 'react-native-permissions';

var markersLength = 0;

class MapComponent extends Component {
    
    constructor(props) {
        super(props);
        this.state = { 
            jobsData: this.props.navigation.getParam('jobsData', ''),
            showUserMarker: false,
            userLatitude: 0,
            userLongitude: 0
         };
    }

    componentDidMount() {
        Permissions.check('location').then(response => {
            if (response === 'authorized') {
                this.getUserLocation();
            }
                
          });
    }

    getUserPermission = () => {
        Permissions.check('location').then(response => {
            if (response === 'authorized') {
                this.getUserLocation();
            }
            else if ((response === 'denied' && Platform.OS === 'ios') || (response === 'restricted' && Platform.OS === 'android') ) {
                this.showPermissionAlert();
            }
            else  {
                console.log('In else, response is: ' + response);
                Permissions.request('location', {
                        rationale: {
                            title: i18n.t('map.permTitle'),
                            message: i18n.t('map.permMesaj')
                        },
                    }).then(response => {
                        if (response === 'authorized') {
                            this.getUserLocation();
                        }
                });
            }
        });      
    }

    

    showPermissionAlert = () => {
        Alert.alert(
            i18n.t('map.permTitle'),
            i18n.t('map.permMesaj'),
            [
                {
                    text: i18n.t('map.buttonNegative'),
                    style: 'cancel',
                  },  
                {text: i18n.t('map.settingsButton'), onPress: () => 
                  {
                    if (Platform.OS === 'ios')
                        Linking.openURL('app-settings:')
                    else {
                        NativeModules.OpenSettings.openLocationSettings(data => {
                            console.log('call back data', data);
                        });
                    }
                  }
                },
            ],
            {cancelable: false},
          );
    }

    getUserLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    showUserMarker: true,
                    userLatitude: position.coords.latitude,//44.4354853
                    userLongitude: position.coords.longitude//26.1026157
                });
                
            },
            (error) => {
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    render() {
        var index = 0;
        var markers = [];
        this.state.jobsData.map(function(item) {        
            markers.push({ 
                 "title" : item.jobtitle,
                 "description"  : item.company,
                 "coordinate": {
                    "latitude"  : item.location.coordinates[1],
                    "longitude"  : item.location.coordinates[0]
                 }
             });
         })
        markersLength = markers.length;
        if (this.state.showUserMarker)
            ++markersLength;//if we don't increment this, the userLocation marker does not display on the map on iOS
        return(
                <View style={StyleSheet.absoluteFillObject}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={styles.map}
                        showsMyLocationButton={false}
                        key={markersLength}
                        region={{
                            latitude: (this.state.userLatitude !== 0 ? this.state.userLatitude : 44.4354853),
                            longitude: (this.state.userLongitude !== 0 ? this.state.userLongitude : 26.1026157),
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}>
                            {markers.map((marker)  => (
                            <Marker
                                key={index++}
                                coordinate={marker.coordinate}>
                                <Callout 
                                    style={styles.calloutStyle}>
                                    <View style={styles.textHeaderStyle}> 
                                            <Text style={styles.titleText}>{marker.title}</Text>
                                    </View>
                                    <View style={styles.descriptionTextView}>
                                        <Text>{marker.description}</Text>
                                    </View>
                                </Callout>
                            </Marker>
                        ))}
                        <View>
                            {
                            this.state.showUserMarker ? 
                                <Marker key={100} 
                                    coordinate={{latitude: this.state.userLatitude,longitude: this.state.userLongitude}} 
                                    icon={require('../../assets/user-location.png')}
                                    /> 
                                : null
                            }
                        </View>
                        </MapView>
                        <Callout style={styles.locationStyle}>
                            <TouchableHighlight 
                                onPress={this.getUserPermission} 
                                underlayColor={'transparent'}
                                style={styles.touchStyle}
                                activeOpacity={0.75}>
                                            <Image 
                                                source={require('../../assets/location-button.png')} 
                                                style={styles.imageStyle}
                                                />
                            </TouchableHighlight>
                        </Callout>
                   </View>
        )
    }
}

export default MapComponent;