import React from 'react';
import {Provider} from 'react-redux';
import {ConfigureStore} from './redux/configureStore';
import {PersistGate} from 'redux-persist/es/integration/react';
import AppNavigator from './components/AppNavigator';


const {persistor, store} = ConfigureStore();
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate
            persistor={persistor}>
            <AppNavigator/>
        </PersistGate>
      </Provider>
    );
  }
}
