export const GET_JOB_TITLES = 'GET_JOB_TITLES';
export const JOB_TITLES_FAILED = 'JOB_TITLES_FAILED';
export const GET_JOBS = 'GET_JOBS';
export const JOBS_LOADING = 'JOBS_LOADING';
export const JOBS_FAILED = 'JOBS_FAILED';