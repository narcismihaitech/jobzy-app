import * as ActionTypes from '../ActionTypes';

export const jobTitles = (state = { errMess: null,
                                 jobTitles:[]}, action) => {
                                     
    switch (action.type) {
        case ActionTypes.GET_JOB_TITLES:
            return {...state, errMess: null, jobTitles: action.payload};

        case ActionTypes.JOB_TITLES_FAILED:
            return {...state, errMess: action.payload};

        default:
          return state;
      }
};