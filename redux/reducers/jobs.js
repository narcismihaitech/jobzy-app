import * as ActionTypes from '../ActionTypes';

const initialState = {
    jobs: [],
    isLoading: true,
    errMess: null
  };

export const jobs = (state = initialState, action) => {
    console.log('In reducer, type is: ' + action.type);                                 
    switch (action.type) {
        case ActionTypes.GET_JOBS:
            return {...state, isLoading: false, errMess: null, jobs: action.payload};

        case ActionTypes.JOBS_LOADING:
            return {...state, isLoading: true, errMess: null, jobs: []}

        case ActionTypes.NO_JOBS_AVAILABLE:
            return {...state, isLoading: false, errMess: null, jobs: []}

        case ActionTypes.JOBS_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
          return state;
      }
};

