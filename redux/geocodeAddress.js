import api_key from '../config/google_api_key';
import {jobsLoading, fetchJobs, jobsFailed} from './ActionCreators';
import {fetchFakeJobs} from '../redux/ActionCreators';
import DEBUG from '../config/debug.js';


export const geocodeAddress = (address, jobTitle) => async (dispatch) => {
    dispatch(jobsLoading());
    if (DEBUG) {
      dispatch(fetchFakeJobs());
    }
    else {
      const apiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&locality=Bucharest&key=' + api_key;
      return fetch(apiUrl)
      .then(response => {
          if (response.ok) {
            return response;
          } else {
            var error = new Error('Geocode error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
          }
        },
        error => {
              var errmess = new Error(error.message);
              throw errmess;
        })
      .then(response => response.json())
      .then(response => dispatch(fetchJobs(response.results[0].geometry.location, jobTitle)))
      .catch(error => dispatch(jobsFailed(error.message)));
    }
    
};

