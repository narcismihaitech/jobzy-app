import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

/** JOB TITLES **/

export const fetchJobTitles = () => async (dispatch) => {
    return fetch(baseUrl + 'api/jobs/job_keywords')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('In fetchJobTitles, Error is ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(jobTitles => dispatch(addJobTitles(jobTitles)))
    .catch(error => dispatch(jobTitlesFailed(error.message)));
};

export const jobTitlesFailed = (errmess) => ({
    type: ActionTypes.JOB_TITLES_FAILED,
    payload: errmess
});

export const addJobTitles = (jobTitles) => ({
    type: ActionTypes.GET_JOB_TITLES,
    payload: jobTitles
});

/** JOBS **/

export const fetchJobs = (address, job) => async (dispatch) => {
  dispatch(jobsLoading());
  
  var addressObj = JSON.parse(JSON.stringify(address));
  var obj = {"origin": [addressObj.lng, addressObj.lat], "job_details": [job]};
  //var obj = {"origin": [26.1410638, 44.4346588], "job_details": ["Developer"]};
  const apiUrl = baseUrl + 'api/jobs/available_jobs';
    return fetch(apiUrl, {
      method: 'POST',
      body: JSON.stringify(obj),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(response => {
        if (response.ok && response.status == 200) {
          return response;
        }
        else {
          var error = new Error('In fetchJobs, Error is: ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(jobs => dispatch(addJobs(jobs)))
    .catch(error => dispatch(jobsFailed(error.message)));
};

/*USED FOR TESTING PURPOSES*/
export const fetchFakeJobs = () => async (dispatch) => {
  console.log('In fetchFakeJobs');
  return fetch('http://localhost:3001/jsonarray')
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          var errmess = new Error(error.message);
          throw errmess;
    })
  .then(response => response.json())
  .then(jobTitles => dispatch(addJobs(jobTitles)))
  .catch(error => dispatch(jobsFailed(error.message)));
};


export const addJobs = (jobs) => ({
  type: ActionTypes.GET_JOBS,
  payload: jobs
});

export const jobsLoading = () => ({
  type: ActionTypes.JOBS_LOADING
});

export const jobsFailed = (errmess) => ({
  type: ActionTypes.JOBS_FAILED,
  payload: errmess
});